(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, Restangular) {
    var vm = this;

    $scope.getUsers = function(){
      var response = Restangular.all('people').getList();
      response.then(function (data) {
        $scope.listPeople = data;
      }).catch(function (err){
        $scope.messageFailure(err);
      });

    }

    $scope.getJobs = function(){
      var response = Restangular.all('jobs').getList();
      response.then(function (data) {
        $scope.listJobs = data;
      }).catch(function (err){
        $scope.messageFailure(err);
      });

  	}
    
    $scope.getJobs();
    $scope.getUsers();

    $scope.messageFailure = function (msg){
      jQuery('.alert-failure-div > p').html(msg);
      jQuery('.alert-failure-div').show();
      jQuery('.alert-failure-div').delay(5000).slideUp(function(){
        jQuery('.alert-failure-div > p').html('');
      });
    }

  }
})();
