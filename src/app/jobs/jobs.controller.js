(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('JobsController', JobsController);

  /** @ngInject */
  function JobsController($scope, Restangular) {
    var vm = this;

  	$scope.saveJob = function(){
      var obj = {
        jobName : $scope.jobName,
        jobSalary : $scope.jobSalary
      };
      var response = Restangular.all('jobs').post(obj);
      response.then(function (data) {
        $scope.listJobs.push({
          id : data.id,
          jobName : data.jobName,
          jobSalary : data.jobSalary
        });
        $scope.clear();
        $scope.messageSuccess('Guardado correctamente');
      }).catch(function (err){
        $scope.messageFailure(err.statusText);
      });

  	}

    $scope.deleteJob = function(id){
      var obj = {id : id};
      var response = Restangular.one('jobs', id).remove();
      response.then(function (data) {
        var index = $scope.listJobs.indexOf(data);
		    $scope.listJobs.splice(index, 1);
        $scope.clear();
        //$scope.messageSuccess('Eliminado correctamente');
      }).catch(function (err){
        $scope.messageFailure(err.statusText);
      });
  	}

  	$scope.getJobs = function(){
      var response = Restangular.all('jobs').getList();
      response.then(function (data) {
        $scope.listJobs = data;
      }).catch(function (err){
        $scope.messageFailure(err);
      });

  	}
    $scope.getJobs();

  	$scope.messageFailure = function (msg){
  		jQuery('.alert-failure-div > p').html(msg);
  		jQuery('.alert-failure-div').show();
  		jQuery('.alert-failure-div').delay(5000).slideUp(function(){
  			jQuery('.alert-failure-div > p').html('');
  		});
  	}

  	$scope.messageSuccess = function (msg){
  		jQuery('.alert-success-div > p').html(msg);
  		jQuery('.alert-success-div').show();
  		jQuery('.alert-success-div').delay(5000).slideUp(function(){
  			jQuery('.alert-success-div > p').html('');
  		});
  	}

    $scope.clear = function(){
      $scope.jobName = '';
      $scope.jobSalary = '';
    }

  }
})();
