(function() {
  'use strict';

  angular
    .module('frontend')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/people', {
        templateUrl: 'app/people/people.html',
        controller: 'PeopleController',
        controllerAs: 'people'
      })
      .when('/jobs', {
        templateUrl: 'app/jobs/jobs.html',
        controller: 'JobsController',
        controllerAs: 'jobs'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
