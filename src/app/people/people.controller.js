(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('PeopleController', PeopleController);

  /** @ngInject */
  function PeopleController($scope, Restangular) {
    var vm = this;

  	$scope.saveUser = function(){
      var obj = {
        firstName : $scope.firstName,
        lastName : $scope.lastName,
        age : $scope.age
      };
      var response = Restangular.all('people').post(obj);
      response.then(function (data) {
        $scope.listPeople.push({
          id : data.id,
          firstName : data.firstName,
          lastName : data.lastName,
          age : data.age,
        });
        $scope.clear();
        $scope.messageSuccess('Guardado correctamente');
      }).catch(function (err){
        $scope.messageFailure(err.statusText);
      });

  	}

    $scope.deleteUser = function(id){
      var obj = {id : id};
      var response = Restangular.one('people', id).remove();
      response.then(function (data) {
        var index = $scope.listPeople.indexOf(data);
		    $scope.listPeople.splice(index, 1);
        $scope.clear();
        //$scope.messageSuccess('Eliminado correctamente');
      }).catch(function (err){
        $scope.messageFailure(err.statusText);
      });
  	}

  	$scope.getUsers = function(){
      var response = Restangular.all('people').getList();
      response.then(function (data) {
        $scope.listPeople = data;
      }).catch(function (err){
        $scope.messageFailure(err);
      });

  	}
    $scope.getUsers();

  	$scope.messageFailure = function (msg){
  		jQuery('.alert-failure-div > p').html(msg);
  		jQuery('.alert-failure-div').show();
  		jQuery('.alert-failure-div').delay(3000).slideUp(function(){
  			jQuery('.alert-failure-div > p').html('');
  		});
  	}

  	$scope.messageSuccess = function (msg){
  		jQuery('.alert-success-div > p').html(msg);
  		jQuery('.alert-success-div').show();
  		jQuery('.alert-success-div').delay(3000).slideUp(function(){
  			jQuery('.alert-success-div > p').html('');
  		});
  	}

    $scope.clear = function(){
      $scope.firstName = '';
      $scope.lastName = '';
      $scope.age = '';
    }


  }
})();
