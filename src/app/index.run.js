(function() {
  'use strict';

  angular
    .module('frontend')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {
    // Log  -- 
    $log.debug('runBlock end');
  }

})();
